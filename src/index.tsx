import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./css/bootstrap.min.css";
import "./css/styles.css";
import "./css/flaticons.css";

ReactDOM.render(<App />, document.getElementById("root"));
