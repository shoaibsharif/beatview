import React from "react";
import { Sidebar } from "./Sidebar";
import { Header } from "./Header";
import { Content } from "./Content";
function App() {
  return (
    <>
      <Sidebar />
      <Header></Header>
      <Content />
    </>
  );
}

export default App;
