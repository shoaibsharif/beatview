import React from "react";
import Logo from "./img/logo.png";
import SupportImage from "./img/support-img.png";

export const Sidebar = () => {
  return (
    <section className="side-bar-area">
      <div className="sidebar-content">
        <a href="/" className="side-bar-logo">
          <img src={Logo} alt="Logo" />
        </a>
        <div className="sidebar-scrolling">
          <div className="sidebar-wrap">
            <div className="side-bar-menu">
              <ul>
                <li>
                  <a href="/">
                    {" "}
                    <i className="flaticon-null" /> <span>Dashboard</span>{" "}
                  </a>
                </li>
                <li>
                  <a href="/">
                    <i className="flaticon-null-1" />
                    <span>Take an interview</span>{" "}
                  </a>
                </li>
                <li>
                  <a href="/">
                    <i className="flaticon-null-3" />{" "}
                    <span>Interview reports</span>{" "}
                  </a>
                </li>
                <li>
                  <a href="/">
                    <i className="flaticon-null-4" />
                    <span>interview code</span>{" "}
                  </a>
                </li>
                <li>
                  <a href="/">
                    {" "}
                    <i className="flaticon-settings" />
                    <span>Setting</span>{" "}
                  </a>
                </li>
                <li>
                  <a href="/">
                    <i className="flaticon-null-8" /> <span>Support</span>{" "}
                  </a>
                </li>
                <li>
                  <a href="/">
                    {" "}
                    <i className="flaticon-null-5" /> <span>Logout</span>{" "}
                  </a>
                </li>
              </ul>
            </div>
            <div className="side-bar-supprt">
              <div className="side-bar-supprt-img">
                <img src={SupportImage} alt="supportImage" />
              </div>
              <div className="side-bar-supprt-content">
                <h3>Support 24/7</h3>
                <p>Contact us anytime</p>
                <button className="beta-btn btn-orange">start</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
