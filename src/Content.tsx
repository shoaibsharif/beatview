import React from "react";
import InterviewPNG from "./img/interview.png";

export const Content = () => {
  return (
    <section className="content-area">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="main-content-area">
              <div className="row">
                <div className="col-md-6">
                  <div className="take-interview-content ">
                    <div className="take-interview-heading">
                      <h2>Take an interview</h2>
                    </div>
                    <div className="take-interview-list">
                      <ul>
                        <li>
                          <a href="">
                            Job interview<span>100 Credit</span>
                          </a>
                        </li>
                        <li>
                          <a href="">
                            School interview<span>120 Credit</span>
                          </a>
                        </li>
                        <li>
                          <a href="">
                            Presentation speech<span>135 Credit</span>
                          </a>
                        </li>
                        <li>
                          <a href="">
                            Orientation speech<span>140 Credit</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="take-interview-img">
                    <img src={InterviewPNG} alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
