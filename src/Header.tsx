import React, { useState } from "react";
import classNames from "classnames";
import LoginLogo from "./img/login-logo.png";
import profileImage from "./img/profile.png";
import { motion } from "framer-motion";

export const Header = () => {
  const [isOpen, toggoleOpen] = useState(false);
  const [isClickedSearchBox, setSearchBox] = useState(false);

  return (
    <header id="top" className="header-area">
      <div className="container-fulid">
        <div className="row">
          <div className="col-12 unset">
            <div
              className={classNames(
                "d-flex justify-content-between align-items-center"
              )}
            >
              <div className="responsive-logo">
                <img src={LoginLogo} alt="" />
              </div>
              <div className="search-box">
                <form
                  onClick={() => setSearchBox(true)}
                  onBlur={() => setSearchBox(false)}
                  className={classNames(
                    "d-flex justify-content-center align-items-center",
                    { active: isClickedSearchBox }
                  )}
                >
                  <i className="flaticon-null-6" aria-hidden="true" />
                  <input
                    className="form-control search-click"
                    type="text"
                    placeholder="Search"
                    aria-label="Search"
                  />
                </form>
              </div>
              <div className="credit d-flex align-items-center">
                <div className="cradet-point d-flex align-items-center">
                  <i className="flaticon-null-14" />
                  <h4>100 Credit</h4>
                </div>
                <button className="beta-btn btn-orange">Add Credit</button>
              </div>
              <div className="profile-area">
                <div
                  className="profile d-flex align-items-center"
                  onClick={() => toggoleOpen(!isOpen)}
                >
                  <div className="profile-name d-flex align-items-center">
                    <img src={profileImage} alt="profile" />
                    <span>Promenusrat</span>
                  </div>
                  <div className="profile-icon">
                    {!isOpen && (
                      <motion.i
                        animate={{
                          clipPath: [
                            "circle(0% at 0% 50%)",
                            "circle(200% at 0% 50%)",
                          ],
                        }}
                        className="flaticon-null-15"
                      />
                    )}

                    {isOpen && (
                      <motion.i
                        animate={{
                          clipPath: [
                            "circle(0% at 0% 50%)",
                            "circle(200% at 0% 50%)",
                          ],
                        }}
                        className="flaticon-null-16"
                      />
                    )}
                  </div>
                </div>
                <motion.div
                  animate={isOpen ? "open" : "close"}
                  initial={false}
                  variants={{
                    open: {
                      clipPath: [
                        "circle(0% at 50% 0%)",
                        "circle(200% at 50% 0%)",
                      ],
                    },
                    close: {
                      clipPath: [
                        "circle(200% at 50% 0%)",
                        "circle(0% at 50% 0%)",
                      ],
                    },
                  }}
                  className="profile-dropdawn"
                >
                  <ul>
                    <li>
                      <a className="d-flex align-items-center">
                        <span>
                          <i className="flaticon-null-17" />
                        </span>
                        Account details{" "}
                        <span className="profile-link-drop">
                          <i className="flaticon-null-13" />
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="#" className="d-flex align-items-center">
                        <span>
                          {" "}
                          <i className="flaticon-null-19" />{" "}
                        </span>
                        Change Password
                      </a>
                    </li>
                    <li>
                      <a href="#" className="d-flex align-items-center">
                        <span>
                          <i className="flaticon-null-14" />
                        </span>
                        Add Payment Method
                      </a>
                    </li>
                    <li>
                      <a href="" className="d-flex align-items-center">
                        <span>
                          <i className="flaticon-null-5" />
                        </span>
                        Logout
                      </a>
                    </li>
                  </ul>
                </motion.div>
              </div>

              <div className="menu-toggole">
                <span className="menu-show comon-tab">
                  <svg
                    version="1.1"
                    id="Capa_1"
                    xmlns="http://www.w3.org/2000/svg"
                    xmlnsXlink="http://www.w3.org/1999/xlink"
                    x="0px"
                    y="0px"
                    viewBox="0 0 384.97 384.97"
                    style={{ background: "new 0 0 384.97 384.97" }}
                    xmlSpace="preserve"
                  >
                    <g>
                      <g id="Menu_1_">
                        <path
                          d="M12.03,120.303h360.909c6.641,0,12.03-5.39,12.03-12.03c0-6.641-5.39-12.03-12.03-12.03H12.03
                                                                                      			c-6.641,0-12.03,5.39-12.03,12.03C0,114.913,5.39,120.303,12.03,120.303z"
                        />
                        <path
                          d="M372.939,180.455H12.03c-6.641,0-12.03,5.39-12.03,12.03s5.39,12.03,12.03,12.03h360.909c6.641,0,12.03-5.39,12.03-12.03
                                                                                      			S379.58,180.455,372.939,180.455z"
                        />
                        <path
                          d="M372.939,264.667H132.333c-6.641,0-12.03,5.39-12.03,12.03c0,6.641,5.39,12.03,12.03,12.03h240.606
                                                                                      			c6.641,0,12.03-5.39,12.03-12.03C384.97,270.056,379.58,264.667,372.939,264.667z"
                        />
                      </g>
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                    </g>
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                  </svg>
                </span>
                <span className="menu-close comon-tab">
                  <svg
                    version="1.1"
                    id="Capa_1"
                    xmlns="http://www.w3.org/2000/svg"
                    xmlnsXlink="http://www.w3.org/1999/xlink"
                    x="0px"
                    y="0px"
                    viewBox="0 0 241.171 241.171"
                    style={{ background: "new 0 0 241.171 241.171" }}
                    xmlSpace="preserve"
                  >
                    <g>
                      <path
                        id="Close"
                        d="M138.138,120.754l99.118-98.576c4.752-4.704,4.752-12.319,0-17.011c-4.74-4.704-12.439-4.704-17.179,0
                                                                                  		l-99.033,98.492L21.095,3.699c-4.74-4.752-12.439-4.752-17.179,0c-4.74,4.764-4.74,12.475,0,17.227l99.876,99.888L3.555,220.497
                                                                                  		c-4.74,4.704-4.74,12.319,0,17.011c4.74,4.704,12.439,4.704,17.179,0l100.152-99.599l99.551,99.563
                                                                                  		c4.74,4.752,12.439,4.752,17.179,0c4.74-4.764,4.74-12.475,0-17.227L138.138,120.754z"
                      />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                    </g>
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                    <g />
                  </svg>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};
